# Planning garde CUR

Documentation de l'application pour créer des listes de gardes.

Date de dernière modification : 03-03-2020

Dévelopeur : Stéphane Tuffier (interne de santé publique, stephane@tuffier.eu)

## Description 

L'objectif de cette application est de créer un planning de garde à partir d'un protocole prenant en compte l'ancienneté et le service d'affectation de chaque interne.
Il a été crée pour faciliter la création du planning de garde des urgences des internes au CHU de Rennes. 

## Jeux de données à utiliser

Deux fichiers sont necessaires pour créer l'application : 

+ le protocole de garde
+ la liste des internes

Des fichiers de test sont fournis par défaut dans l'application. Il est possible de les télécharger dans l'onglet *Procole de garde* et *Nombre de garde par internes* pour s'en servir de modèle. Il est possible d'importer des nouveaux fichiers.

### Protocole de garde

Le tableau décrivant du protocole de garde doit contenir un minimum de 3 colonnes : `service`, `week_end`, `s_x`.

+ `service` : texte indiquant un service d'affectation possible pour les internes ou un statut particullier (ex interchu ou autre). C'est à partir de cette colonne que le nombre de garde à faire sera calculé pour chaque interne.
+ `week_end` : `TRUE`(oui) ou `FALSE`(non). Indique si dans ce service les internes doivent faire des gardes les weekends lorsqu'ils sont à 50% de garde. 
+ `s_x` : numérique. Indique la proportion (coefficient) de gardes à faire selon l'ancienneté dans ce service. L'internat de médecine pouvant durer 10 semestre, il faut créer 10 colonnes (il est possible de rajouter d'autres colonnes si besoin) : `s1`	`s2`	`s3`	`s4`	`s5`	`s6`	`s7`	`s8`	`s9`	`s10`.  Lorsqu'on utilise Excel il faut bien vérifier que cette colonne est reconnue comme un nombre ([documentation excel](https://support.office.com/fr-fr/article/convertir-les-nombres-stock%C3%A9s-en-tant-que-texte-en-nombres-40105f2a-fe79-4477-a171-c5bad0f0a885)). Trois valeurs sont possibles : 
    + 1 lorsque la proportion de garde est de 100%, 
    + 0.5  pour une proportion de 50% 
    + 0 ou *case_vide* (*NA*) si il n'y a pas de garde à faire. 

Le protocole utilisé par défaut est celui de l'année 2019 du CHU de Rennes (si le protocole n'a pas évolué, il n'est pas necessaire de le modifier).

### Liste des internes

Ce fichier doit contenir trois colonnes :  `nom`, `service`, `semestre`

+ `nom` : nom et prénom identifiant chaque interne participant aux gardes. Dans le cas ou des internes aurait la même identité (même nom et prénom) il faut utiliser un identifiant unique en rajoutant la date de naissance par exemple.
+ `service` : service d'affectation ou statut de chaque internes pour le semestre à venir. Cette colonne doit correspondre au texte utilisé dans la colonne `service` du fichier protocole **sans fautes d'orthographe ou de chagement de cases** (minuscules ou majuscules). 
+ `semestre` : Ancienneté de l'interne danc un format numérique. Cette colonne sera utilisée pour calculer le nombre de garde à faire selon les colonnes `s_x` du fichier protocole.

### Paramètre de l'application

L'application permet de modifier rapidement des paramètres influencant le planning :

+ **Nombres de lignes de gardes** : le nombre de lignes de garde que l'application doit créer. Au CHU de Rennes c'est 2 lignes de garde (2 internes présents aux urgences chaque soir).
+ **Jours de repos entre chaques gardes** : nombre de jours sans garde après une garde pour un même interne.
+ **Nombre de semestre à rajouter** : nombre de semestre à ajouter à la colonne semestre de la liste des internes pour obtenir l'ancienneté qu'aurons les internes sur la période du planning.
    +  +2 si utilisation du nombre de semestre validé,
    +  +1 pour semestre actuel
    +  +0 pour le semestre futur.
+ **Date du planning** : date de début et de fin du planning, format YYYY-MM-DD.

## Fonctionnement

L'application va d'abord calculer plusieurs paramètres en croisant le fichier protocole avec la liste des internes :

+ le coefficient de garde pour chaque interne ainsi que son statut pour les weekend (`coef` dans le tableau final). 
+ la somme de ces coefficients (`somme_coef`)
+ le nombre total de garde à faire calculé selon la durée du planning et le nombre de ligne de garde (`n_jours * n_liste`)

A partir de ces paramètres le nombre de garde à faire pour chaque interne est calculé en rapportant le nombre total de garde à faire à la somme des coefficients (nombre de gardes pour un interne à 100%) multiplié par le coefficient de l'interne (colonne `todo` du tableau finale, `todo = coef * (n_jours * n_liste) / somme_coef`). L'aglorithme va ensuite tirer au sort un interne parmis ceux disponibles pour chaque jour (hors période de repos, et hors week-end pour ceux n'ayant pas les week_end). L'algorithme peut tirer un interne jusqu'à ce son nombre de gardes à faire soit dépassé. 

Les résultats sont exportés en deux tableaux, un pour le planning et un pour le récapitulatif du nombre de gardes plannifiée pour chaque interne (`done`) et des différents paramètres (service, semestre, nom, coef, todo, error, week_end).

## Problèmes de répartitions

Dans certains cas il se peut qu'il n'y ai pas assez d'internes pour effectuer toutes les gardes. Cela peut arriver si il y a trop listes, un nombre de jours de repos trop important par rapport au nombre d'internes faible. Le planning comportera alors des lignes vides et les internes auront un nombre de garde inférieure à celui qu'il auraient du faire (`coef` > `done`). Dans ce cas il faut réduire le nombre de jours de repos ou de listes.

Des internes peuvent être exclus de la constitution du planning par exemple à cause d'erreurs d'orthographe ou de matching entre les deux fichiers. Ces erreurs sont détaillées dans la colonne `error` de l'onglet *Liste d'attribution (avec erreur)*. Pour les corriger il faut modifier les fichiers *Protocole* et *Liste des internes* et les importer à nouveau dans l'application.